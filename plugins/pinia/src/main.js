/...
import { createApp } from 'vue'
+import { createPinia } from 'pinia'
...
const app = createApp(App)
+app.use(createPinia())
.../
